#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <unistd.h>
#include <sys/time.h>
#include <likwid.h>

unsigned int dimensao;
unsigned int bandas;
char *nome_output;
FILE *output;
unsigned int max_iter;
double tolerancia;


/* PROTOTIPOS DAS FUNCOES */
double f(double x);
int generateRandomDiagonal( unsigned int N, unsigned int k, unsigned int nBandas, double *diag );
void grava_arquivo(double *vetor_solucao, double *res, double *erro_res, double *cg_time, double *res_time, unsigned int n_iteracoes);
double max_vetor(double *v, unsigned int n);
double media_aritmetica_vetor(double *v, unsigned int n);
double min_vetor(double *v, unsigned int n);
void multiplica_matriz_vetor(double *A, double *v, double *x, unsigned int n);
double norma_residuo (double *v, unsigned int n);
double produto_escalar(double *u, double *v, unsigned int n);
double timestamp ();
void valida_parametros (int argc,  char * const argv[]);
void verify(double *A, double *b, double *x);
