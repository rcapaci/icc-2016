#include "lib.h"

int main(int argc, char * const argv[])
{
    char buf[1024];
    unsigned int i, j, k, kMax;
    double *A, *x, *b, *soma, *err;


    /*inicializa gerador de numeros aleatorios */
    srand( 20162 );

    /* valida parametros do programa */
    valida_parametros(argc, argv);
    kMax = 2 * bandas + 1;


    do {
        fgets(buf, 1024, stdin);
        printf("%s", buf);
    } while (buf[0] == '#');

    dimensao = atoi(buf);
    printf("dimensao = %u\n", dimensao);

    A = malloc(sizeof(double) * dimensao * dimensao);
    x = malloc(sizeof(double) * dimensao);
    b = malloc(sizeof(double) * dimensao);
    soma = malloc(sizeof(double) * dimensao);
    err = malloc(sizeof(double) * dimensao);
    // char c =getc(stdin);

    for (i = 0; i < dimensao; i++) {
        scanf("%s", buf);
        x[i] = atof(buf);
        printf("%.14g\n", x[i]);
    }


    // inicializa todos os vetores, conforme o algoritmo
    //  x = 0; r = b; v = b;
    for (i = 0; i < dimensao; i++) {
        for (j = 0; j < dimensao; j++) {
            A[i*dimensao+j] = 0.0;
        }
        b[i] = f(i * M_PI/dimensao);
    }

    // gera sequencia de numeros "aleatorios" e coloca na matriz
    double *diag = malloc(sizeof(double) * (dimensao));
    for (k = 0; k <= bandas; k++) {
        if (generateRandomDiagonal(dimensao, k, kMax, diag) != 0) {
            fprintf(stderr, "Erro na geração de diagonais. Um dos seguintes erros podem ter acontecido:\n");
            fprintf(stderr, "Dimensao < 3\n");
            fprintf(stderr, "Número de bandas > Dimensao/2\n");
            fprintf(stderr, "Banda atual < 0\n");
            fprintf(stderr, "Banda atual > número de bandas\n");
            exit(1);
        }

        i = 0;
        j = k;
        while (i < dimensao - k && j < dimensao) {
            A[i*dimensao + j] = diag[i];
            A[j*dimensao + i] = diag[i];
            i++;
            j++;
        }
    }
    free(diag);


    // for (i=0;i<dimensao;i++) {
    //     for (j=0;j<dimensao;j++) {
    //         printf("%.15lf  ", A[i*dimensao+j]);
    //     }
    //     printf("%.15lf        ", x[i]);
    //     printf("%.15lf  ", b[i]);
    //     puts(" ");
    // }




    // z = A*v
    multiplica_matriz_vetor(A, x, soma, dimensao);
    for (i=0;i<dimensao;i++) {
        err[i] = fabs(b[i] - soma[i]);

        printf("i=%d  %.15lf  =  %.15lf   -   %.15lf\n", i, err[i], b[i], soma[i]);
    }

    free(A);
    free(x);
    free(b);
    free(err);


    return 0;
}
