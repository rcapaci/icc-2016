#include "lib.h"

/**
 * Funcao f(x)
 *
 * retorna 4π² ( sin(2πx) + sin(2π(π-x)) )
 */
double f(double x)
{
    return 4 * pow(M_PI, 2) * ( sin(2 * M_PI * x) + sin(2 * M_PI * (M_PI - x)));
}

/**
 * N: tamanho do sistema linear
 * k: numero da diagonal, 0 = diagonal principal, 1 = acima/abaixo da diagonal, 2 = ...
 * nBandas: numero de bandas do sistema linear
 * diag: vetor para armazenar os valores da diagonal. Deve ser alocado por quem chama a função.
 */
int generateRandomDiagonal( unsigned int N, unsigned int k, unsigned int nBandas, double *diag )
{
    if ( !diag || N < 3 || nBandas > N/2 || k < 0 || k > nBandas )
    return (-1);

    /* garante valor dominante para diagonal principal */
    double fator = (k == 0) ? ((double)(nBandas-1)) : (0.0);

    double invRandMax = 1.0 / (double)RAND_MAX;

    for (int i=0; i < N-k; ++i)
    {
        diag[i] = fator + (double)rand() * invRandMax;
    }

    return (0);
}

/**
 * Grava informacoes obtidas no metodo no arquivo de saida
 *
 * vetor_solucao: vetor solucao encontrado pelo metodo GC
 * res: vetor que guarda a norma do residuo a cada iteracao do metodo GC
 * erro_res: vetor que guarda o erro aproximado no calculo da norma do residuo
 *           a cada iteracao
 * t_gc: vetor que guarda o tempo de execucao do metodo GC a cada iteracao
 * t_res: vetor que guarda o tempo de execucao do calculo da norma do residuo
 *        a cada iteracao
 * n_iteracoes: numero de iteracoes executadas pelo metodo GC
 */
void grava_arquivo(double *vetor_solucao, double *res, double *erro_res, double *t_gc, double *t_res, unsigned int n_iteracoes)
{
    int i, j;
    fprintf(output,"###########\n");
    fprintf(output, "# Tempo Método CG: %.14g %.14g %.14g\n", min_vetor(t_gc, n_iteracoes), media_aritmetica_vetor(t_gc, n_iteracoes), max_vetor(t_gc, n_iteracoes));
    fprintf(output, "# Tempo Resíduo: %.14g %.14g %.14g\n#\n", min_vetor(t_res, n_iteracoes), media_aritmetica_vetor(t_res, n_iteracoes), max_vetor(t_res, n_iteracoes));
    fprintf(output, "# Norma Euclidiana do Residuo e Erro aproximado\n");

    for (i = 0; i < n_iteracoes; i++) {
        fprintf(output, "# i=%d: %.14g %.14g\n", i+1, res[i], erro_res[i]);
    }
    fprintf(output, "###########\n");
    fprintf(output, "%d\n", dimensao);

    for (i = 0; i < dimensao; i++) {
        fprintf(output,"%.14g ", vetor_solucao[i]);
    }
}

/**
 * Retorna o maior valor em um vetor
 *
 * v: vetor
 * n: tamanho do vetor
 */
double max_vetor(double *v, unsigned int n)
{
    int i, max;
    max = v[0];
    for (i = 1; i < n; i++) {
        if (v[i] > max) {
            max = v[i];
        }
    }
    return max;
}

/**
 * Retorna a media aritmetica dos valores em um vetor
 *
 * v: vetor
 * n: tamanho do vetor
 */
double media_aritmetica_vetor(double *v, unsigned int n)
{
    int i;
    double soma, c, y, t;

    //executa a soma de kahan, para minimizar erros
    soma = 0.0;
    c = 0.0;
    for (i=0; i < n; i++) {
        y = v[i] - c;
        t = soma + y;
        c = (t - soma) - y;
        soma = t;
    }

    return soma/n;
}

/**
 * Retorna o menor valor em um vetor
 *
 * v: vetor
 * n: tamanho do vetor
 */
double min_vetor(double *v, unsigned int n)
{
    int i, min;
    min = v[0];
    for (i=1; i < n; i++) {
        if (v[i] < min) {
            min = v[i];
        }
    }
    return min;
}

/**
 * Multiplica uma matriz simétrica, de bandas, por um vetor
 *
 * A: vetor que guarda as bandas de uma matriz simétrica e de bandas
 * v: vetor que multiplica a matriz A
 * x: vetor que guarda o resultado da multiplicacao
 * n: dimensao
 */
void multiplica_matriz_vetor(double *A, double *v, double *x, unsigned int n)
{
    LIKWID_MARKER_START("matriz-vetor");


    double c, y, t, soma;
    unsigned int i, j, k;
    for (k=0; k < dimensao; k++) {
        soma = 0.0;
        c = 0.0;
        for (i=1,j=k-1; i <= k && i<= bandas/2; i++,j--) {
            y = (A[i*dimensao+j] * v[j]) - c;
            t = soma + y;
            c = (t-soma) - y;
            soma = t;
        }
        for (i=1,j=k+1; i<= bandas/2 && j<dimensao; i++,j++) {
            y = (A[i*dimensao+k] * v[j]) - c;
            t = soma + y;
            c = (t-soma) - y;
            soma = t;
        }
        y = (A[k] * v[k]) - c;
        t = soma + y;
        c = (t-soma) - y;
        soma = t;

        x[k] = soma;
    }
LIKWID_MARKER_STOP("matriz-vetor");
}

/**
 * Retorna a norma L2 de um vetor
 *
 * v: vetor da qual sera calculada a norma
 * n: tamanho do vetor v
 */
double norma_residuo (double *v, unsigned int n)
{
    double soma, c, t, y;
    unsigned int i;

    // executa soma de kahan, para minimizar erros
    soma = 0.0;
    c = 0.0;
    for (i = 0; i < n; i++){
        y = (v[i]*v[i]) - c;
        t = soma + y;
        c = (t - soma) -y;
        soma = t;
    }

    return (sqrt(soma));
}

/**
 * Retorna o produto escalar entre dois vetores
 *
 * u: vetor de double
 * v: vetor de double
 * n: tamanho dos vetores u e v
 */
double produto_escalar(double *u, double *v, unsigned int n)
{
    int i;
    double c, t, y, resultado;
    resultado = 0.0;
    c = 0.0;
    for (i=0; i<n; i++) {
        y = (u[i] * v[i]) - c;
        t = resultado + y;
        c = (t-resultado) - y;
        resultado = t;
    }
    return resultado;
}

/**
 * Retorna o tempo atual em mili-segundos
 */
double timestamp ()
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return((double)(tp.tv_sec*1000.0 + tp.tv_usec/1000.0));
}

/**
 * Trata os argumentos passados ao programa na chamada da execucao
 *
 * Os parametros dimensao, bandas e nome_output sao obrigatorios. Caso nao sejam
 * definidos, o programa termina sua execucao, com uma mensagem de erro.
 *
 * Os parametros maxIter e tolerancia sao opcionais e, caso nao sejam definidos,
 * usamos alguns valores default:
 *
 */
void valida_parametros (int argc,  char * const argv[])
{
    int index;
    int c;
    nome_output = NULL;
    dimensao = strtoul(argv[1], NULL, 10);
    bandas = strtoul(argv[2], NULL, 10);
    opterr = 0;
    while ((c = getopt (argc, argv, "i:t:o:")) != -1) {
        if (c == 'i') {
            max_iter = strtoul(optarg, NULL, 10);
        } else if (c == 't') {
            tolerancia = atof(optarg);
        } else if (c == 'o') {
            nome_output = optarg;
        } else if (c == '?') {
            if (optopt == 'i' || optopt == 't' || optopt == 'o') {
                fprintf (stderr, "Não existe argumento para a opção -%c.\n", optopt);
            } else {
                fprintf (stderr, "Opção desconhecida: `-%c'.\n", optopt);
            }
            exit(1);
        } else {
            abort ();
        }
    }

    if (!nome_output) {
        fprintf (stderr, "Não foi definido arquivo de saída.\n");
        exit(1);
    }

    if (!dimensao) {
        fprintf (stderr, "Não foi definido a dimensão do sistema.\n");
        exit(1);
    }

    if (!bandas) {
        fprintf (stderr, "Não foi definido o numero de bandas do sistema.\n");
        exit(1);
    }

    if (!max_iter) {
        max_iter = dimensao;
    }

    if (!tolerancia) {
        tolerancia = DBL_MIN;
    }
}

/**
 * Verifica o erro da solucao encontrada pelo metodo GC, calculando b-Ax
 */
void verify(double *A, double *b, double *x)
{
    double *z = malloc(sizeof(double)*dimensao);
    double err;
    unsigned int i;

    // z = A*v
    multiplica_matriz_vetor(A, x, z, dimensao);
    for (i=0;i<dimensao;i++) {
        err = fabs(b[i] - z[i]);
        printf("i=%d  %.14g  =  %.14g   -   %.14g\n", i, err, b[i], z[i]);
    }

    free(z);
}
