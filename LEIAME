################################################################################
#######              AUTORIA DO SOFTWARE
################################################################################

	- Aline Santana Cordeiro       GRR20120712
	- Rafael Capaci Pereira        GRR20120731


################################################################################
#######              ARQUIVOS DO PACOTE
################################################################################

Lista dos arquivos contidos no pacote:

    - lib.h: biblioteca do trabalho

    - functions.c: arquivo que contém funcões auxiliares utilizadas, como
	tratamento de parametros de entrada e gravação de arquivo, cálculo de produto
	escalar, multiplicação de matriz por vetor, cálculo da norma do resíduo,
	função f(x), valores máximo e mínimo e média de um vetor, etc.

	- main.c: programa principal, contém a implementação do método do Gradiente
	Conjugado

	- Makefile: makefile para a compilação do programa


Lista dos arquivos que serão gerados no pacote:

	- cgSolver: executável do programa gerado pela compilação

	- Arquivo de saída com informações sobre os valores calculados pelo método


################################################################################
#######              ALGORITMO UTILIZADO E ESTRUTURA DE DADOS
################################################################################

	Inicialmente, utilizamos de estrutura somente vetores dinamicamente alocados
para a execução do método, ou seja, para alocar as bandas da matriz, o vetor
solução, os resíduos, os erros do resíduo, entre outros. Também são definidas
variáveis globais para receber os parâmetros do programa.

	Para executar o programa, são passados alguns parâmetros como dimensão da
matriz, número de bandas, número máximo de iterações, tolêrancia do resíduo e
arquivo de saída. Para isto, foi criada a função
			void valida_parametros(int argc,  char * const argv[]),
que trata de cada um destes parâmetros:
	- Caso não seja definido o número máximo de iterações ou a tolerância do
	erro, são usados valores default, ou seja, para o número máximo de iterações
	é usada a dimensão da matriz e para a tolerância DBL_MIN que é a representação
	positiva do menor valor possível em double.
	- Caso não seja definido o arquivo de saída, é mostrada uma mensagem avisando
	que não foi definido arquivo de saída.
	- Caso os parâmetros não sejam passados corretamente, ou seja, -i para número
	máximo de iterações, -t para tolerância e -o para o arquivo de saída, uma
	mensagem de erro é gerada.

	Para gerar a matriz, simétrica definida positiva e altamente esparsa,
utiilizamos um vetor de tamanho:
 						(bandas/2)+1 * dimensao
de forma a guardar todas as bandas da matriz, gerada pelo algoritmo dado pelo
Professor, sequencialmente no vetor. Por exemplo, a matriz A será armazenada no
vetor V da seguinte forma:

						|a11 a12 a13  0   0  ...|
						|a21 a22 a23 a24  0  ...|
					A =	|a31 a32 a33 a34 a35 ...|
						| 0  a42 a43 a44 a45 ...|
						| 0	  0  a53 a54 a55 ...|

  V = [a11, a22, a33, a44, a55, a21, a32, a43, a54, 0, a31, a42, a53, 0, 0 ...]

	Os zeros em V existem para manter um padrão mais simples de busca/acesso dos
valores no vetor. Os algoritmos que utilizam desse acesso serão descritos nas
próximas seções.

	Para calcular o vetor b, é utilizada a função dada pelo Professor, onde:
		f(x) = 4π² ( sin(2πx) + sin(2π(π-x)) )

		e b[i] = f(i * M_PI/dimensao), para cada i = 0, 1, ..., dimensao - 1;

	Depois de armazenados os valores, o método é iniciado. Seguimos o algoritmo
do Gradiente Conjugado apresentado no livro texto. Para isto, utilizamos as
seguintes funções:
	double produto_escalar(double *u, double *v, int n)
	void multiplica_matriz_vetor(double *A, double *v, double *x, int n)

	Como solicitado pelo Professor, fizemos o cálculo do tempo de cada iteração
do método e, separadamente, calculamos a norma do resíduo e o tempo de execução
dela e, por fim, considerando a norma, fizemos o cálculo do erro aproximado.
Para isto, as seguintes funções foram utilizadas:
	double timestamp ()
	double norma_residuo (double *resid, int n)

	A variável 'norma_ant' guarda o valor da norma do resíduo na iteração
anterior e, como inicialmente não temos nenhum valor para a norma, a definimos
como zero.

	Por fim, é gravado no arquivo de saída:
		-o menor tempo, a média e o maior tempo das iterações do método e do
		resíduo.
		- a Norma Euclidiana e o erro para cada iteração
		- e o vetor solucao
	São usadas as funções:
	void grava_arquivo(double *vetor_solucao, double *res, double *erro_res,
						double *cg_time, double *res_time, int n_iteracoes)
	double min_vetor(double *v, int n)
	double media_vetor(double *v, int n)
	double max_vetor(double *v, int n)

	O método converge até atingir o número de dimensões da matriz ou até atingir
a tolerância.


- ALGORITMO PARA MULTIPLICAÇÃO DE MATRIZ POR VETOR

	Consideremos o exemplo a seguir, A*x = b, onde:

			|a00 b01 c02  0   0  ...|			|x0|			|b0|
			|b10 a11 b12 c13  0  ...|			|x1|			|b1|
		A =	|c20 b21 a22 b23 c24 ...|		x = |x2|		b = |b2|
			| 0  c31 b32 a33 b34 ...|			|x3|			|b3|
			| 0	  0  c42 b43 a44 ...|			|x4|			|b4|

	Sabendo que as bandas são simétricas, ou seja, b10 = b01, b21 = b12, ...,
podemos simplificar, de forma que podemos renomeá-las b10 = b01 = b0
													  b21 = b12 = b1
													  b32 = b23 = b2
													  ...
													  b[i][j] = b[j][i] = b[j]
													  c[i][j] = c[j][i] = c[j]
													  ...

Da mesma forma a[i][i] = ai, já que é a diagonal principal.

	Para encontrar a solução da multiplicação, devemos fazer as seguintes operações:

				b0 = a0*x0 + b0*x1 + c0*x2
				b1 = b0*x0 + a1*x1 + b1*x2 + c1*x3
				b2 = c0*x0 + b1*x1 + a2*x2 + b2*x3 + c2*x4
				b3 =		 c1*x1 + b2*x2 + a3*x3 + b3*x4
				b4 =				 c2*x2 + b3*x3 + a4*x4

	Como guardamos as bandas em um vetor, podemos organizá-las visualmente da
seguinte forma:

	 					M|	0 |	1  | 2	| 3	 | 4
						0| a0 | a1 | a2 | a3 | a4
						1| b0 | b1 | b2 | b3 |  0
						2| c0 | c1 | c2 |  0 |  0

	Seja M a matriz ilustrativa da ordem das bandas, com índices M[2, 4], onde
m00 = a0, m01 = a1, ..., as operações para encontrar a solução, descritas acima,
podem ser reescritas da seguinte forma:

				b0 = m00*x0 + m10*x1 + m20*x2
				b1 = m10*x0 + m01*x1 + m11*x2 + m21*x3
				b2 = m20*x0 + m11*x1 + m02*x2 + m12*x3 + m22*x4
				b3 = 		  m21*x1 + m12*x2 + m03*x3 + m13*x4
				b4 = 		  		   m22*x2 + m13*x3 + m04*x4

	A partir desta visualização com a matriz M, podemos encontrar um padrão de
execução, o qual podemos generalizar nos seguintes somatórios:

		for (k = 0; k < dimensao; k++) {
			for (i = 0, j = k; i <= k && i <= bandas/2; i++,j--)
				b[k] += A[i*dimensao+j] * x[j];
			for (i = 1, j = k+1; i <= bandas/2 && j < dimensao; i++,j++)
		        b[k] += A[i*dimensao+k] * x[j];
		}

	Logo, podemos ver que ao longo do vetor, todas as bandas dispoem do mesmo
número de índices que a diagonal principal dispoe, contudo, como têm menos
variáveis, são alocados zeros para completar o número de índices de cada banda,
isto é feito para simplificar a lógica de acesso aos valores, como já foi citado
acima. Apesar de manter números sem efeito, são alocados bem menos zeros que
seria alocado em uma matriz.


################################################################################
#######              ALTERNATIVAS DE IMPLEMENTAÇÃO
################################################################################

	A prinicípio, estavamos alocando a matriz de bandas em uma estrutura própria
de matriz, mas isto acabava deixando o método um pouco mais lento, fora isto,
armazenava muitos valores nulos sem necessidade, ocupando espaço e fazendo
operações desnecessárias. Posteriormente, conseguimos criar o algoritmo citado
acima para realizar as operaçẽs necessárias com a matriz em um único vetor,
armazenando somente as bandas dela.

	O algoritmo que apresentamos, foi o que conseguimos na teoria. Na prática,
funciona de maneira ligeiramente diferente, pois foram adotados algumas medidas
a fim de evitar erros numéricos.
	A primeira medida foi remover a multiplicação que envolve o elemento da
diagonal principal de dentro do somatório, somando somente no fim, pois, como a
matriz é diagonalmente dominante, os valores da diagonal principal possuem uma
magnitude maior que os elementos das outras diagonais, o que pode acabar gerando
cancelamento subtrativo em alguma operação e ir acumulando erro, caso essa
operação continue dentro do somatório.

	Além disso, também utilizamos a Soma de Kahan nas situações onde
necessitávamos somar uma sequência de números. O algoritmo da soma de Kahan
reduz o erro gerado por arredondamento e truncamento de valores e foi utilizado
em situações diversas, como na multiplicação de matriz por vetor, no produto
escalar e no cálculo da norma. Após a utilização do algoritmo, observamos uma
melhora sutil nos resultados das operações.


################################################################################
#######              DIFICULDADES ENCONTRADAS
################################################################################

	- Entender o método do Gradiente Conjugado foi difícil, o que tomou certo
	tempo do desenvolvimento.

	- Dificuldade em entender o cálculo do resíduo, pois usamos a implementação
	apresentada no livro texto, a qual retornava um resíduo bastante pequeno,
	contudo, foi solicitado pelo Professor a utilização da norma e, a princípio,
	entendemos que deveriamos substituir o produto escalar do método pela norma,
	o que retornava o erro bastante grande, depois compreendemos que o método
	deveria ser mantido e a norma calculada à parte.

	- Estávamos aplicando o método com o valor de tolerância default grande.
	Quando o programa era executado sem a tolerância como argumento, nós
	adotávamos FLT_EPSILON como tolerância default. Devido à isso, o método
	terminava antes das n iterações, depois que ajustamos a tolerância default
	para DBL_MIN, o método obteve respostas melhores.

	- Foi bastante complicado elaborar o algoritmo para alocar as bandas da
	matriz em um vetor, foi uma das partes que mais tomou tempo. Por isso
	havíamos feito tudo utilizando uma matriz nxn mesmo, para ter algo que
	pudesse ser entregue.


################################################################################
#######              BUGS CONHECIDOS
################################################################################
