#include "lib.h"

int main(int argc, char * const argv[]) {
        LIKWID_MARKER_INIT;

    double t_res_begin, t_gc_begin, t_res_end, t_gc_end, norma_ant = 0.0;
    unsigned int i, j, k;

    // inicializa gerador de numeros aleatorios
    srand( 20162 );

    // valida parametros do programa
    valida_parametros(argc, argv);
    output = fopen(nome_output, "w");

    /**
    * A:n*bandas guarda as bandas da matriz
    * x:n vetor solucao
    * b:n
    * v:n vetor auxiliar
    * r:n vetor residuo do metodo do GC
    * z:n vetor auxiliar
    * res:max_iter guarda a norma do residuo a cada iteracao
    * t_gc:max_iter guarda o tempo de execucao do metodo a cada iteracao
    * t_res:max_iter guarda o tempo de execucao do calculo da norma do residuo a cada iteracao
    * err_res:max_iter guarda o erro aproximado do calculo do residuo a cada iteracao
    *
    * OBS: nos comentarios, um vetor x' é o vetor transposto de x
    */
    double  s, m, aux, aux1;
    double *A, *x, *b, *v, *r, *z, *res, *t_res, *t_gc, *err_res;
    A = malloc(sizeof(double) * (int)((bandas/2)+1) * dimensao);
    x = malloc(sizeof(double) * dimensao);
    b = malloc(sizeof(double) * dimensao);
    r = malloc(sizeof(double) * dimensao);
    v = malloc(sizeof(double) * dimensao);
    z = malloc(sizeof(double) * dimensao);
    res = malloc(sizeof(double) * max_iter);
    t_gc = malloc(sizeof(double) * max_iter);
    t_res = malloc(sizeof(double) * max_iter);
    err_res = malloc(sizeof(double) * max_iter);

    // zera todas as posições da matriz A
    for (i=0;i<=bandas/2;i++) {
        for (j=0;j<dimensao;j++) {
            A[i*bandas+j] = 0.0;
        }
    }

    /**
     * Gera sequencia de numeros "aleatorios" e coloca na matriz
     *
     * k = 0, diagonal principal
     * k > 0, k-esima diagonal acima da diagonal principal
     */
    double *diag = malloc(sizeof(double) * (dimensao));
    for (k = 0; k <= bandas/2; k++) {
        if (generateRandomDiagonal(dimensao, k, bandas, diag) != 0) {
            fprintf(stderr, "Erro na geração de diagonais. Um dos seguintes erros podem ter acontecido:\n");
            fprintf(stderr, "Dimensao < 3\n");
            fprintf(stderr, "Número de bandas > Dimensao/2\n");
            fprintf(stderr, "Banda atual < 0\n");
            fprintf(stderr, "Banda atual > número de bandas\n");
            exit(1);
        }

        j = 0;
        while (j <  dimensao - k) {
            A[k*dimensao + j] = diag[j];
            j++;
        }
    }
    free(diag);

    /**** INICIO DO METODO ****/

    // inicializa todos os vetores, conforme o algoritmo e a especificacao
    // x = 0; r = b; v = b;
    for (i = 0; i < dimensao; i++) {
        x[i] = 0.0;
        z[i] = 0.0;
        err_res[i] = 0.0;
        b[i] = f(i * M_PI/dimensao);
        v[i] = b[i];
        r[i] = b[i];
    }

    // aux = r' * r
    aux = produto_escalar(r, r, dimensao);

    for (k = 0; k < max_iter; k++) {

        // inicia a contagem do tempo de cada iteracao do metodo
        t_gc_begin = timestamp();

        // z = A*v
        multiplica_matriz_vetor(A, v, z, dimensao);

        // s = aux/ (v'*z)
        s = aux / produto_escalar(v, z, dimensao);

        // x = x + s*v
        // r = r - s*z
        for (i = 0; i < dimensao; i++) {
            x[i] = x[i] + s * v[i];
            r[i] = r[i] - s * z[i];
        }

        // aux1 = r' * r
        aux1 = produto_escalar(r, r, dimensao);

        m = aux1/aux;
        aux = aux1;

        // v = r + m*v
        for (i = 0;i<dimensao;i++) {
            v[i] = r[i] + m * v[i];
        }

        // termino da iteracao, finaliza contagem de tempo e guarda no vetor
        t_gc_end = timestamp();
        t_gc[k] = t_gc_end - t_gc_begin;

        // calcula a norma e o tempo de execucao
        t_res_begin = timestamp();
        res[k] = norma_residuo(r, dimensao);
        t_res_end = timestamp();
        t_res[k] = t_res_end - t_res_begin;

        // calcula o erro aproximado baseado na norma
        err_res[k] = fabs(res[k] - norma_ant);
        norma_ant = res[k];

        // se norma do residuo < tolerancia, acaba o metodo
        if (fabs(err_res[k]) <= tolerancia) {
            break;
        }
    }

    // Executa a verificacao do vetor solucao (b-Ax)
    // verify(A,b,x);
    grava_arquivo(x, res, err_res, t_gc, t_res, k == max_iter ? k : k+1);

    free(A);
    free(x);
    free(b);
    free(v);
    free(r);
    free(z);
    free(res);
    free(err_res);
    free(t_res);
    free(t_gc);
    fclose(output);
LIKWID_MARKER_CLOSE;

    return 0;
}
